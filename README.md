![Logo FIUSAC](./imagenes/logo.png)
# Modelacion y Simulación 2

## Proyecto - Fase #1
## Universidad de San Carlos de Guatemala


### Integrantes Grupo #20
| Carnet | Nombre |
| ------ | ------ |
| 201503750 | Carlos Eduardo Carías Salan |
| 201504042 | Julio Estuardo Gómez Alonzo  |
| 201503840 | Hugo René Figueroa Castillo |


# Modelo Principal
En el modelo principal se agregaron los elementos 'Server' segun la ubicación brindada en el archivo *'DataFile.xlsx'* en la hoja de calculo *'Current State Inventory Policy'*. Tambien se agrego la imagen de fondo del 'Layout Transparent', y podemos observar que los objetos concuerdan correctamente con los apartados de Inventory de la imagen.

![Logo FIUSAC](./imagenes/modelo_principal.png)

## Proceso de lectura de la BOM Matrix
El proceso para la lectura de la matriz BOM Matrix se realizó con los 'Step' de *Excel Read*. Creando un ciclo anidado a otro para poder ir variando la columna por cada fila para lograr leer todos los valores de la matriz.

![Logo FIUSAC](./imagenes/lectura_bmatrix.png)

## Proceso de lectura de datos en Excel
El proceso para la lectura de los datos en el archivo proporcionado se realizó con los 'Step' de *Excel Read*. Creando un ciclo anidado que lee linea por linea empezando desde la columna que nos interesa asignando los valores a states especificos ( ver en seccion states utilizados ).

![Logo FIUSAC](./imagenes/PROCESOREADDATA.PNG)

## States utilizados
Los estados utilizados para esta fase son los siguientes:

![Logo FIUSAC](./imagenes/states_principal.png)

- **linea**: almacena el valor de la fila actual de la lectura de la Bom Matrix.
- **valor**: almacena el valor actual de la Bom Matrix.
- **Producto**: almacena el valor del encabezado del Producto de la Bom Matrix.
- **Componente**: almacena el valor del encabezado del Componente de la Bom Matrix.
- **columna**: almacena el valor de la columna actual de la lectura de la Bom Matrix.
- **contador**: contador para llevar el control del total de datos leidos de la Bom Matrix.

### States para modelo de datos
![Logo FIUSAC](./imagenes/States_datos.png)

- Los Strings Producto, Estacion, StartTime, EndTime, Supplier, Material, OrderDate, InspectionStart, InspectionEnd, ReceptionDate hacen referencia a las mismas columnas en el archivo de excel de datos.
- Los Strings Tiempo1, Tiempo2 nos ayudan a hacer un substring para obtener la hora de los datos
- Los Integer Hora1, Minuto1, Segundo1, Hora2, Minuto2, Segundo2, SumaTime1, SumaTime2, ResultadoTime nos sivren para hacer los calculos de diferencia de tiempo para la distribucion

## Descripción del análisis
### Software utilizado: 
- Rstudio

### Gráficas:
![Graficas](./imagenes/analisis.png)
Las gráficas nos mostraban el comportamiento de los datos analizados y nos permitian elegir la distribución que más se asociaba a dicho comportamiento de cada grupo de datos.

### Comandos:
``` 
    library(dplyr)
    library(MASS)
    library(Survival)
    library(fitdistrplus)
    library(rriskDistributions)
    library(xlsx)
``` 
Esta sección de código es en la que importamos todas las librerias a utilizar, desde las que nos permiten analizar los datos hasta la librería con la que vamos a escribir el archivo de salida con los valores especificados.

``` 
    Product<-c()
    Estation<-c()
    Distrib<-c()
    Min<-c()
    Max<-c()
    Media<-c()
    Desviacion<-c()
``` 
Esta es la sección en la que se crean las variables en las cuales se van a guardar los datos a imprimir en el archivo de excel y se inicializan como un vector vacio.

```
    for(i in 1:length(productos)){
        for(j in 1:length(estaciones)){
            temporal=filter(datos, Producto == productos[i],Estacion==estaciones[j])
            if(length(temporal$Resultado)>0){
            Product<-c(Product,productos[i])
            Estation<-c(Estation,estaciones[j])
            Min<-c(Min,min(temporal$Resultado))
            Max<-c(Max,max(temporal$Resultado))
            Media<-c(Media,mean(temporal$Resultado))
            Desviacion<-c(Desviacion,sd(temporal$Resultado))
            dist<-fit.cont(temporal$Resultado)
            Distrib<-c(Distrib,dist$chosenDistr)
            combinaciones=combinaciones+1
            }
        }
    }
```
Este es el corazón el programa ya que es el encargado de recorrer todas las combinaciones posibles entre los atributos como las estaciones por cada producto y los valores que nos retornan para designarle una distribución e ingresar cada valor en su respectivo vector para posteriormente escribirlos en el documento.

```
totales <- data.frame(Estation,Product,Distrib,Min,Max,Media,Desviacion)
write.xlsx(totales, file = "Datos1.xls", sheetName = "Datos",append=FALSE)
```
Para finalizar el código se crea un data frame con los vectores previamente ingresados y se procede a escribir el resultante en el archivo de excel.


## Conclusiones

Al analizar los tres distintos archivos de datos concluimos que:

-  Algunos grupos de datos no cumplen con ninguna distribución, por lo que tuvimos que legir la que más se pareciera aunque no cumpliera con todos lo requisitos.

- A pesar de que muchos grupos de datos tenian varias distribuciones que se le podrían aplicar, las distribuciones más comunes o las que se le pueden asociar a más fácilmente son la distribución normal y la distribución cauchy.